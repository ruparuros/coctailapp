//
//  DataManager.swift
//  CoctailApp
//
//  Created by Uros Rupar on 11/13/23.
//

import Foundation

class DataManager {
    static func fetchData<T: Decodable>(url: String, type: T.Type, completion: @escaping (T?) -> Void) {
         
         guard let url = URL(string: url) else {
             return
         }
         
         URLSession.shared.dataTask(with: url) { data, response, error in
             guard let data = data else {
                 completion(nil)
                 return
             }
             
             do {
                 let features = try JSONDecoder().decode(T.self, from: data)
                 completion(features)
             } catch {
                 completion(nil)
             }
         }.resume()
     }
    
    static func fetchRemoteConfigg(url: String, completion: @escaping (DrinkData?) -> Void) {
         guard let url = URL(string: url) else {
             return
         }

         URLSession.shared.dataTask(with: url) { data, response, error in
             guard let data = data else {
                 completion(nil)
                 return
             }
             
             do {
                 let features = try JSONDecoder().decode(DrinkData.self, from: data)
                 completion(features)
             } catch {
                 completion(nil)
             }
         }.resume()
     }
    
    static func fetchTrendingMovies(url: String, completion: @escaping (DrinkData?) -> Void){
        
        guard let apiURL = URL(string: url) else { return }
        let task = URLSession.shared.dataTask(with: apiURL){ (data, response, error) in
            
            if let error = error {
                print("Error accesing Coctail Api: \(error)")
            }
            
            guard let httpResponse = response as? HTTPURLResponse,
                  (200...299).contains(httpResponse.statusCode) else {
                print("Error with response, uninspected status code: \(response)")
                return
            }
            if let data = data, case let movies = try? JSONDecoder().decode(DrinkData.self, from: data){
                completion(movies)
            }
        }
        task.resume()
    }

}
