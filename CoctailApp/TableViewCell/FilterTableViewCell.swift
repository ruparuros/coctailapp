//
//  FilterTableViewCell.swift
//  CoctailApp
//
//  Created by Uros Rupar on 11/15/23.
//

import UIKit

class FilterTableViewCell: UITableViewCell {

    @IBOutlet weak var arrowIcon: UIImageView!
    @IBOutlet weak var filterParametarTitleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        filterParametarTitleLabel.setDefaultFont(fontSize: 16)
        filterParametarTitleLabel.textColor = .black
    }

  

}
