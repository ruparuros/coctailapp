//
//  CustomTextField.swift
//  CoctailApp
//
//  Created by Uros Rupar on 11/17/23.
//

import UIKit

@IBDesignable
class CustomTextField: UITextField {

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        self.font = UIFont(name: "caveat-regular", size: 13) ?? UIFont.systemFont(ofSize: 13)
        self.textAlignment = .center
        self.backgroundColor = .white
        let grayText = NSAttributedString(string: "My Placeholder",
                                                          attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        self.attributedPlaceholder = grayText
        self.textColor = .black
    }
    
    func setPlaceholder(text: String) {
        let grayText = NSAttributedString(string: text,
                                                          attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        self.attributedPlaceholder = grayText
        self.textColor = .black
    }
    
    func addICon(action: UIAction? = nil, image: UIImage) {
        let button = UIButton(type: .custom)
        button.setImage(image, for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        button.frame = CGRect(x: CGFloat(self.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
        if let action = action {
            button.addAction(action, for: .touchUpInside)
        }
        self.rightView = button
        self.rightViewMode = .always
    }
}
