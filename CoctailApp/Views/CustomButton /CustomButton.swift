//
//  CustomButton.swift
//  CoctailApp
//
//  Created by Uros Rupar on 11/17/23.
//

import UIKit

class CustomButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func styleForDefaultButton(title: String) {
        let atributedString : [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font:  UIFont(name: "caveat-regular", size: 12),
            NSAttributedString.Key.foregroundColor: UIColor.brownColor
        ]
        let attributeString = NSMutableAttributedString(
            string: title,
            attributes: atributedString
        )
        
        self.setAttributedTitle(attributeString, for: .normal)
        
        
        self.backgroundColor = .white
        let icon = UIImage(named: "registerButtonIcon")
        self.setImage(icon, for: .normal)
        
        self.setInsets(forContentPadding: UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0), imageTitlePadding: 20)
    }
    
    func styleForSecondaryButton(title: String) {
        let atributedString : [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font:  UIFont(name: "caveat-regular", size: 15),
            NSAttributedString.Key.foregroundColor: UIColor.white
        ]
        let attributeString = NSMutableAttributedString(
            string: title,
            attributes: atributedString
        )
        
        self.setAttributedTitle(attributeString, for: .normal)
        self.backgroundColor = .white.withAlphaComponent(0.1)
        self.tintColor = .white
        //self.colo
        let icon = UIImage(named: "exit")
        let renderImage = icon?.withRenderingMode(.alwaysTemplate)
        self.setImage(renderImage, for: .normal)
        self.setInsets(forContentPadding: UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0), imageTitlePadding: 20)
    }
}
