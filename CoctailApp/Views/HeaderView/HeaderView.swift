//
//  HeaderView.swift
//  CoctailApp
//
//  Created by Uros Rupar on 11/22/23.
//

import UIKit

class HeaderView: UICollectionReusableView {

    @IBOutlet weak var wrapperView: UIView!
    @IBOutlet weak var parametarLabel: UILabel!
    //@IBOutlet weak var wrapperView: UIView!
    
  
    
    override init(frame: CGRect) {
       super.init(frame: frame)
       self.backgroundColor = UIColor.purple
        parametarLabel.setDefaultFont(fontSize: 12)
        wrapperView.layer.cornerRadius = 9
       // Customize here

    }

    required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)

    }
    
    func setCell(){
        parametarLabel.setDefaultFont(fontSize: 15)
        wrapperView.layer.cornerRadius = 4
        self.backgroundColor = .mainColor
    }
 
}
