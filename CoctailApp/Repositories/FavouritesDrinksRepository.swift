//
//  FavouritesDrinksRepository.swift
//  CoctailApp
//
//  Created by Uros Rupar on 11/14/23.
//

import Foundation
import RealmSwift

class FavouritesDrinksRepository {
    
   static let realm = try! Realm()
    
    func saveDrinkInFavourites(drink: DrinkRealm) {
        
        do {
            try FavouritesDrinksRepository.realm.write {
                FavouritesDrinksRepository.realm.add(drink)
            }
        } catch {
            print("Error saving \(error)")
        }
    }
    
    func loadDrinks() -> Results<DrinkRealm> {
        return FavouritesDrinksRepository.realm.objects(DrinkRealm.self)
    }
    
    func loadDrinksByUser(userEmail: String) -> Results<DrinkRealm> {
        return loadDrinks().where {$0.user.email == userEmail }
    }
    
    func isUserAdedToWatchList(drink: DrinkRealm, userEmail: String) -> Bool {
        return loadDrinksByUser(userEmail: userEmail).filter { $0.strDrink == drink.strDrink}
            .count > 0
    }
}
