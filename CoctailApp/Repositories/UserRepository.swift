//
//  UserRepository.swift
//  CoctailApp
//
//  Created by Uros Rupar on 11/14/23.
//

import Foundation
import RealmSwift

class UserRepository {
    static let realm = try! Realm()
    let drinksRepository = FavouritesDrinksRepository()
    
     func saveUser(user: UserRealm) {
         
         do {
             try UserRepository.realm.write {
                 UserRepository.realm.add(user)
             }
         } catch {
             print("Error saving \(error)")
         }
     }
    
    func updateUser(user: UserRealm, email: String) {
        
        do {
            try UserRepository.realm.write {
                
                user.email = email
            }
        } catch {
            print("Error saving \(error)")
        }
    }
    
    func loadUsers() -> Results<UserRealm> {
        return UserRepository.realm.objects(UserRealm.self)
    }
    
    func loadDrinks(user: UserRealm) -> List<DrinkRealm>{
        return user.drinks
    }

    func getUserByEmail(email: String) ->  Results<UserRealm> {
        let users = loadUsers()
        return users.where { $0.email == email }
    }
    
    func addDrink(drink: DrinkRealm, user: UserRealm){
        do {
            try UserRepository.realm.write {
                user.addDrinks(drink: drink)
            }
        } catch {
            print(error)
        }
    }
    
    func removeDrink(drink: DrinkRealm, user: UserRealm) {
        do {
            try UserRepository.realm.write {
                user.removeDrinks(drink: drink)
            }
        } catch {
            print(error)
        }
    }
}


