//
//  CoctailCollectionViewCell.swift
//  CoctailApp
//
//  Created by Uros Rupar on 11/13/23.
//

import UIKit
import Kingfisher

protocol DrinkFavouritesDelegate {
    func appendToFavourites(drink: DrinkRealm)
    func removeFromFavourites(drink: DrinkRealm)
}

class CoctailCollectionViewCell: UICollectionViewCell {
    
    var delegate: DrinkFavouritesDelegate?
    var isAdded: Bool!
    var drink: DrinkRealm!
    let drinksRepository = FavouritesDrinksRepository()
    let useServce = UserService()
    @IBOutlet weak var coctailImage: UIImageView!
    @IBOutlet weak var coctailName: UILabel!
    
    @IBOutlet weak var heartIcon: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        coctailName.setDefaultFont(fontSize: 15)
        heartIcon.setImage(UIImage(named: "favourites1"), for: .normal)
        layer.masksToBounds = false
            layer.shadowRadius = 4
            layer.shadowOpacity = 1
            layer.shadowColor = UIColor.gray.cgColor
            layer.shadowOffset = CGSize(width: 0 , height: 2)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
     
    }
    
    func setCell(drink: Drink) {
        self.drink = DrinkRealm()
        self.drink.idDrink = drink.idDrink
        self.drink.strDrink = drink.strDrink
        self.drink.strDrinkThumb = drink.strDrinkThumb
        isAdded = drinksRepository.isUserAdedToWatchList(drink: self.drink, userEmail: useServce.loadCurrentUser()!.username)
        if let url = URL(string: drink.strDrinkThumb){
            coctailImage.kf.setImage(with: url)
        }
        var image = ""
        image = isAdded ? "favourites2" : "favourites1"
        heartIcon.setImage(UIImage(named: image), for: .normal)
        coctailName.textColor = .red
        coctailName.text = drink.strDrink
        coctailName.setDefaultFont(fontSize: 15)
        
    }
    
    func setCell(drink: DrinkRealm) {
        self.drink = drink
        isAdded = drinksRepository.isUserAdedToWatchList(drink: self.drink, userEmail: useServce.loadCurrentUser()!.username)
        if let url = URL(string: drink.strDrinkThumb){
            coctailImage.kf.setImage(with: url)
        }
        var image = ""
        image = isAdded ? "favourites2" : "favourites1"
        heartIcon.setImage(UIImage(named: image), for: .normal)
        coctailName.textColor = .red
        coctailName.text = drink.strDrink
        coctailName.setDefaultFont(fontSize: 12)
    }
    
    @IBAction func addToWatchList(_ sender: Any) {
        
        if isAdded {
            heartIcon.setImage(UIImage(named: "favourites1"), for: .normal)
            self.delegate?.removeFromFavourites(drink: self.drink)
            isAdded = false
        } else {
            heartIcon.setImage(UIImage(named: "favourites2"), for: .normal)
            self.delegate?.appendToFavourites(drink: self.drink)
            isAdded = true
        }
    }
}
