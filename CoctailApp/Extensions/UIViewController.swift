//
//  UIViewController.swift
//  CoctailApp
//
//  Created by Uros Rupar on 11/13/23.
//

import Foundation
import UIKit

extension UIViewController {
    
    
    func setNavigatonWithRightButtons() {
        setNavigaton()
        setStatusBarColor()
        //setRightSearchButton(action: #selector(goToAccountScreen), target: self, image: "list.bullet")
        setRightSearchButtons()
    }
    
    func setNavigaton() {
        self.navigationController?.navigationBar.backgroundColor = .mainColor
        let atributedString : [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font: UIFont(name: "Caveat-Bold", size: 20),
            NSAttributedString.Key.foregroundColor: UIColor.black
          ]
        navigationItem.backBarButtonItem?.setTitleTextAttributes([ NSAttributedString.Key.font: UIFont(name: "Caveat-Bold", size: 15)!], for: .normal)
        navigationController?.navigationBar.tintColor = .black
        navigationController?.navigationBar.titleTextAttributes = atributedString
        
        if #available(iOS 13.0, *) {
            let navBarAppearance = UINavigationBarAppearance()
            let attributes = [NSAttributedString.Key.font: UIFont(name: "Caveat-Bold", size: 20)!, NSAttributedString.Key.foregroundColor: UIColor.black]
            navBarAppearance.titleTextAttributes = attributes
            navBarAppearance.buttonAppearance.normal.titleTextAttributes = attributes
            navBarAppearance.doneButtonAppearance.normal.titleTextAttributes = attributes
            navBarAppearance.backgroundColor = .mainColor
            self.navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
            self.navigationController?.navigationBar.standardAppearance = navBarAppearance
        }

        setStatusBarColor()
    }
    
    
    func setStatusBarColor(){
        guard let frame = navigationController?.navigationBar.bounds else { return }
        let statusBarHeight = UIApplication.shared.statusBarFrame.size.height
        let statusBarGradient = UIView(frame: CGRect(x: 0, y: 0, width: frame.width, height: statusBarHeight))
        statusBarGradient.backgroundColor = .mainColor
        view.addSubview(statusBarGradient)
    }
    
    
    func setRightSearchButton(action: Selector? = nil, target: AnyObject? = nil, image: String ) {
        let rightButton = UIBarButtonItem(title: "", style: .plain, target: target, action: action)
        rightButton.image = UIImage(systemName: image)
        rightButton.tintColor = .black
        self.navigationItem.setRightBarButton(rightButton, animated: true)
    }
    
    func setRightSearchButtons() {
        let searchButton = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(goToSearchVC))
        searchButton.image = UIImage(named: "search")
        searchButton.tintColor = .black
        let filterButton = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(goToFilterVC))
        filterButton.image = UIImage(named: "dash")
        filterButton.tintColor = .black
        self.navigationItem.setRightBarButtonItems([searchButton, filterButton], animated: true)
    }
    
    func showAlert(title: String,message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func goToFilterVC() {
        if let navigationController = navigationController {
            navigationController.pushViewController(FilterVC.instantiate(), animated: true)
        }
    }
    
    @objc func goToSearchVC() {
        if let navigationController = navigationController {
            navigationController.pushViewController(SearchVC.instantiate(), animated: true)
        }
    }
    
    func descendants<T>(type: T.Type? = nil) -> [T] {
        var childrenOfType: [T] = []
        self.children.forEach {
            if let child: T = ($0 as? T) {
                childrenOfType.append(child)
            }
            if !$0.children.isEmpty {
                childrenOfType += $0.descendants(type: type)
            }
        }
        return childrenOfType
    }
    
    func setMainBackground() {
        self.view.applyGradient(colors: [UIColor.mainColor.cgColor,UIColor.orangeColor.cgColor,], locations: [0.0, 1.0], opacity: 1)
    }
    
    func setLoginBackground() {
        self.view.applyGradient(colors: [UIColor.greenLightColor.cgColor,UIColor.greenDarkColor.cgColor,], locations: [0.0, 1.0], opacity: 1)
    }
    
    func setRegisterBackground() {
        self.view.applyGradient(colors: [UIColor.creamColor.cgColor,UIColor.brownColor.cgColor,], locations: [0.0, 1.0], opacity: 1)
    }
}
