//
//  String.swift
//  CoctailApp
//
//  Created by Uros Rupar on 11/13/23.
//

import Foundation

extension String {
    
    func localized(withLanguageFrom userDefaults: UserDefaults = UserDefaults.standard, argument: String? = nil) -> String {
       
        var path = Bundle.main.path(forResource: "en", ofType: "lproj")
        if path == nil {
            path = Bundle.main.path(forResource: "hr", ofType: "lproj")
        }
        let bundle = Bundle(path: path!)
        
        let localizedString = NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
        if let argument = argument {
            return String(format: localizedString, argument)
        } else {
            return localizedString
        }
    }
}
