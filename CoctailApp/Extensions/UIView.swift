//
//  UIView.swift
//  CoctailApp
//
//  Created by Uros Rupar on 11/16/23.
//

import Foundation
import UIKit

extension UIView {
    
    func applyGradient(colors: [CGColor], locations: [NSNumber], opacity: Float, startPoint: CGPoint = CGPoint(x: 0.5, y: 0.0), endPoint: CGPoint = CGPoint(x: 0.5, y: 1.0)) {
        let gradient = CAGradientLayer()
        gradient.colors = colors
        gradient.locations = locations
        gradient.frame = self.bounds
        gradient.opacity = opacity
        gradient.startPoint = startPoint
        gradient.endPoint = endPoint
        layer.insertSublayer(gradient, at: 0)
    }
    
    func addTopBorderWithColor(color: UIColor, width: CGFloat) {
       let border = CALayer()
        border.backgroundColor = color.cgColor
       border.frame = CGRectMake(0, 0, self.frame.size.width, width)
       self.layer.addSublayer(border)
     }

     func addRightBorderWithColor(color: UIColor, width: CGFloat) {
       let border = CALayer()
         border.backgroundColor = color.cgColor
       border.frame = CGRectMake(self.frame.size.width - width, 0, width, self.frame.size.height)
       self.layer.addSublayer(border)
     }

     func addBottomBorderWithColor(color: UIColor, width: CGFloat) {
       let border = CALayer()
         border.backgroundColor = color.cgColor
       border.frame = CGRectMake(0, self.frame.size.height - width, self.frame.size.width, width)
       self.layer.addSublayer(border)
     }

     func addLeftBorderWithColor(color: UIColor, width: CGFloat) {
       let border = CALayer()
         border.backgroundColor = color.cgColor
       border.frame = CGRectMake(0, 0, width, self.frame.size.height)
       self.layer.addSublayer(border)
     }
}
