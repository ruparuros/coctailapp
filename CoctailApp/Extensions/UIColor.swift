//
//  UIColor.swift
//  CoctailApp
//
//  Created by Uros Rupar on 11/13/23.
//

import Foundation
import UIKit

extension UIColor {
    
    static func colorWithRGBValues(redValue: CGFloat, greenValue: CGFloat, blueValue: CGFloat, alpha: CGFloat = 1.0) -> UIColor {
        return UIColor(red: redValue/255.0, green: greenValue/255.0, blue: blueValue/255.0, alpha: alpha)
    }
    
    public class var mainColor: UIColor {
        return UIColor.colorWithRGBValues(redValue: 251, greenValue: 241, blueValue: 189)
    }
    
    public class var secondary: UIColor {
        return UIColor.colorWithRGBValues(redValue: 223, greenValue: 112, blueValue: 67)
    }
    
    public class var orangeColor: UIColor {
        return UIColor.colorWithRGBValues(redValue: 234, greenValue: 153, blueValue: 34)
    }
    
    public class var greenLightColor: UIColor {
        return UIColor.colorWithRGBValues(redValue: 189, greenValue: 232, blueValue: 213)
    }
    public class var greenDarkColor: UIColor {
        return UIColor.colorWithRGBValues(redValue: 54, greenValue: 104, blueValue: 44)
    }
    
    public class var creamColor: UIColor {
        return UIColor.colorWithRGBValues(redValue: 231, greenValue: 206, blueValue: 143)
    }
    
    public class var brownColor: UIColor {
        return UIColor.colorWithRGBValues(redValue: 74, greenValue: 20, blueValue: 19)
    }
    
    public class var customGrayColor: UIColor {
        return UIColor.colorWithRGBValues(redValue: 212, greenValue: 205, blueValue: 165)
    }
    
}
