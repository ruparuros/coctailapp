//
//  UILabel.swift
//  CoctailApp
//
//  Created by Uros Rupar on 11/15/23.
//

import Foundation
import UIKit

extension UILabel {
    
    func setDefaultFont(fontSize:CGFloat) {
        self.font = UIFont(name: "caveat-regular", size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
        self.textColor = UIColor.black
    }
    
    func setDefaultBoldFont(fontSize:CGFloat, color: UIColor) {
        self.font = UIFont(name: "Caveat-Bold", size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
        self.textColor = color
    }
}
