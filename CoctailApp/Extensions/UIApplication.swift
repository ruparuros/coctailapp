//
//  UIApplication.swift
//  CoctailApp
//
//  Created by Uros Rupar on 11/16/23.
//

import Foundation
import UIKit

extension UIApplication {
    class func getController<T: UIViewController>(of type: T.Type) -> UIViewController? {
        return UIApplication.shared.keyWindow?.rootViewController?.descendants(type: type).first
    }
}
