//
//  DrinkData.swift
//  CoctailApp
//
//  Created by Uros Rupar on 11/13/23.
//

import Foundation
import RealmSwift

// MARK: - DrinkData
struct DrinkData: Codable {
    let drinks: [Drink]
}

// MARK: - Drink
struct Drink: Codable {
    let strDrink: String
    let strDrinkThumb: String
    let idDrink: String
}

class DrinkRealm: Object {
    
    override init() {
        
    }
    
    internal init(strDrink: String = "", strDrinkThumb: String = "", idDrink: String = "", user: LinkingObjects<UserRealm> = LinkingObjects(fromType:UserRealm.self , property: "drinks")) {
        self.strDrink = strDrink
        self.strDrinkThumb = strDrinkThumb
        self.idDrink = idDrink
        self.user = user
    }
    
    
    @objc dynamic var strDrink: String = ""
    @objc dynamic var strDrinkThumb: String = ""
    @objc dynamic var idDrink: String = ""
    var user = LinkingObjects(fromType:UserRealm.self , property: "drinks")
}

class UserRealm: Object {
    @objc dynamic var email: String = ""
    var drinks = List<DrinkRealm>()
    
    func addDrinks(drink: DrinkRealm) {
        self.drinks.append(drink)
    }
    
    func removeDrinks(drink: DrinkRealm) {
        let index = self.drinks.firstIndex { $0.strDrink == drink.strDrink }
        self.drinks.remove(at:index!)
    }
}
