//
//  FilterData.swift
//  CoctailApp
//
//  Created by Uros Rupar on 11/16/23.
//

import Foundation

struct FilterData: Codable {
    var drinks: [FilterCategory]
}

struct FilterCategory: Codable {
    var strCategory: String?
    var strGlass: String?
    var strIngredient1: String?
    var strAlcoholic: String?
}

