//
//  RegistrationVC.swift
//  CoctailApp
//
//  Created by Uros Rupar on 11/13/23.
//

import UIKit

class RegistrationVC: UIViewController {

    private(set) var userService: UserService!
    let repository = UserRepository()
    
    @IBOutlet weak var captionLabel: UILabel!
    @IBOutlet weak var nameTF: CustomTextField!
    @IBOutlet weak var emailTF: CustomTextField!
    @IBOutlet weak var passwordTF: CustomTextField!
  
    @IBOutlet weak var backToLogin: CustomButton!
    @IBOutlet weak var registerButton: CustomButton!
    @IBOutlet weak var repeatPasswodTF: CustomTextField!
    @IBOutlet weak var registerImage: UIImageView!

    @IBOutlet weak var orLabel: UILabel!
    class var identifier: String { "RegistrationVC" }
    
    class func instantiate(userService: UserService) -> RegistrationVC {
        let storyboard = UIStoryboard(name: "Authentication", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: identifier) as! RegistrationVC
        vc.userService = userService
        return vc
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setRegisterBackground()
        self.registerImage.image = UIImage(named: "register")
        captionLabel.setDefaultBoldFont(fontSize: 23, color: .black)
        captionLabel.text = "Register"
        initializeUI()
    }
    
    @IBAction func register(_ sender: Any) {
        
        let result = userService.registerUser(name: nameTF.text!, username: emailTF.text!, password: passwordTF.text!, repeatPassword: repeatPasswodTF.text!)
        
        switch result {
        case .success(let user):
                let userRealm =  UserRealm()
                userRealm.email = user.username
                repository.saveUser(user: userRealm)
                presentMainUIFlow()
        case .failure(let error): handleError(error)
            
        }
    }
    
    @IBAction func backToLogin(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func  initializeUI(){
        registerButton.styleForDefaultButton(title: "register")
        backToLogin.styleForSecondaryButton(title: "back to login")
        orLabel.setDefaultBoldFont(fontSize: 24, color: .white)
        orLabel.text = "Or"
        emailTF.setPlaceholder(text: "email")
        passwordTF.setPlaceholder(text: "password")
        repeatPasswodTF.setPlaceholder(text: "password")
        nameTF.setPlaceholder(text: "name")
    }
    
    func presentMainUIFlow(){
        let vc = MainTabBarVC.instantiate(userService: self.userService)
        vc.modalPresentationStyle = .fullScreen
        vc.modalTransitionStyle = .crossDissolve
        present(vc, animated:false,completion: nil)
    }
    
    func handleError(_ error:Error){
        print(error.localizedDescription)
        let alert = UIAlertController(title: "info", message:error.localizedDescription, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
