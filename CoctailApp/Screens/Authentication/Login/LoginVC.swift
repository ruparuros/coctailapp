//
//  LoginVC.swift
//  CoctailApp
//
//  Created by Uros Rupar on 11/13/23.
//

import UIKit

class LoginVC: UIViewController {
    
    private(set) var userService: UserService!
    class var identifier: String { "LoginVC" }
    
    @IBOutlet weak var captionLabel: UILabel!
    @IBOutlet weak var emailTF: CustomTextField!
    @IBOutlet weak var passwordTF: CustomTextField!
    @IBOutlet weak var loginButton: CustomButton!
    @IBOutlet weak var loginImage: UIImageView!
    @IBOutlet weak var orLabel: UILabel!
    @IBOutlet weak var goToRegister: CustomButton!
    
 
    
    class func instantiate(userService: UserService) -> LoginVC {
        let storyboard = UIStoryboard(name: "Authentication", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: identifier) as! LoginVC
        vc.userService = userService
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLoginBackground()
        self.loginImage.image = UIImage(named: "login")
        initializeUI()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func goToRegister(_ sender: Any) {
        let vc = RegistrationVC.instantiate(userService: self.userService)
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func loginUser(_ sender: Any) {
        
        let result = userService.login(username: self.emailTF.text!, password: passwordTF.text!)
        
        switch result {
        case .success(let user): presentMainUIFlow()
            
        case .failure(let error): handleError(error)
            
        }
    }
    
   
    
    func  initializeUI(){
        loginButton.styleForDefaultButton(title: "Login")
        goToRegister.styleForSecondaryButton(title: "go to register")
        orLabel.setDefaultBoldFont(fontSize: 24, color: .white)
        orLabel.text = "Or"
        captionLabel.setDefaultBoldFont(fontSize: 23, color: .black)
        captionLabel.text = "Login"
        emailTF.setPlaceholder(text: "email")
        passwordTF.setPlaceholder(text: "password")
    }
    
    func validateForm()->Bool{
        if let pass = self.passwordTF.text,!pass.isEmpty,
           let usn = self.emailTF.text,!usn.isEmpty
        {
            return true
        }
        
        return false
    }
    
    func presentMainUIFlow(){
        let vc = MainTabBarVC.instantiate(userService: self.userService)
        
        vc.modalPresentationStyle = .fullScreen
        vc.modalTransitionStyle = .crossDissolve
        present(vc, animated:false,completion: nil)
    }
    
    func handleError(_ error:Error){
        let alert = UIAlertController(title: "info", message:error.localizedDescription, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
