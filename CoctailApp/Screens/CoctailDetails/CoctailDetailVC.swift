//
//  CoctailDetailVC.swift
//  CoctailApp
//
//  Created by Uros Rupar on 11/23/23.
//

import UIKit

class CoctailDetailVC: UIViewController {

    class var identifier: String { "CoctailDetailVC" }
    var drink: DrinksDetails?
    
    @IBOutlet weak var instructionsTextView: UITextView!
    @IBOutlet weak var coctailNameLabel: UILabel!
    @IBOutlet weak var posterImage: UIImageView!
    class func instantiate(drink: Drink) -> CoctailDetailVC {
        let storyboard = UIStoryboard(name: "Utils", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: identifier) as! CoctailDetailVC
        DataManager.fetchData(url: "https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i=\(drink.idDrink)", type: DrinksDetailsData.self) { data in
            vc.drink = data?.drinks.first
            
            DispatchQueue.main.async {
                vc.setUI()
            }
        }
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    func setUI() {
        self.setMainBackground()
        self.setNavigaton()
        title = drink?.strDrink
        if let url = URL(string: drink!.strDrinkThumb ?? ""){
            posterImage.kf.setImage(with: url)
        }
        coctailNameLabel.text = drink?.strDrink
        coctailNameLabel.setDefaultBoldFont(fontSize: 23, color: .black)
        instructionsTextView.font  = UIFont(name: "Caveat-regular", size: 14) ?? UIFont.systemFont(ofSize: 14)
        
//        let fixedWidth = instructionsTextView.frame.size.width
//        let newSize = instructionsTextView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
//        instructionsTextView.frame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        instructionsTextView.isScrollEnabled = false
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
