//
//  FavouritesVC.swift
//  CoctailApp
//
//  Created by Uros Rupar on 11/13/23.
//

import UIKit
import RealmSwift

class FavouritesVC: UIViewController {
    private(set) var userService: UserService!
    let repository = UserRepository()
    let drinksRepository = FavouritesDrinksRepository()
    @IBOutlet weak var favouritesCollectionView: UICollectionView!
    var favouritesDrinks: List<DrinkRealm>!
    class var identifier: String { "FavouritesVC" }
    
    class func instantiate(userService: UserService) -> FavouritesVC {
        let storyboard = UIStoryboard(name: "Utils", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: identifier) as! FavouritesVC
        
        vc.userService = userService
        return vc
    }
    
    class func instantiateWithNavigation(userService: UserService) -> UINavigationController {
        let vc = instantiate(userService: userService)
        
        let navigation = UINavigationController(rootViewController: vc)
        
        
        return navigation
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigatonWithRightButtons()
        
        title = "faouritesTitle"~
        let nib = UINib(nibName: "CoctailCollectionViewCell", bundle: nil)
        favouritesCollectionView.register(nib, forCellWithReuseIdentifier: "CoctailCollectionViewCell")
        setMainBackground()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        favouritesDrinks = repository.getUserByEmail(email: userService.loadCurrentUser()?.username ?? "").first?.drinks ?? List<DrinkRealm>()
        DispatchQueue.main.async {
            self.favouritesCollectionView.reloadData()
        }
    }
}

extension FavouritesVC: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        favouritesDrinks.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CoctailCollectionViewCell", for: indexPath) as! CoctailCollectionViewCell
        
        cell.setCell(drink: (favouritesDrinks[indexPath.row]))
        cell.delegate = self
        return cell
    }
}

extension FavouritesVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = Double(self.view.frame.width/2 - 10)
        let height = 253.0
        return CGSize(width: width, height: height)
    }
    
}

extension FavouritesVC: DrinkFavouritesDelegate {
   
    func appendToFavourites(drink: DrinkRealm) {
        let currentUserEmail = userService.loadCurrentUser()?.username
        
        let user = repository.getUserByEmail(email: currentUserEmail ?? "")
        let userRealm = UserRealm()
        userRealm.drinks = user.first!.drinks
        userRealm.email = user.first!.email
        repository.addDrink(drink: drink, user:userRealm )
    }

    func removeFromFavourites(drink: DrinkRealm) {
        let currentUserEmail = userService.loadCurrentUser()?.username
        let user = repository.getUserByEmail(email: currentUserEmail ?? "")
        let userRealm = UserRealm()
        userRealm.drinks = user.first!.drinks
        userRealm.email = user.first!.email
        repository.removeDrink(drink: drink, user: userRealm)
    }

}
