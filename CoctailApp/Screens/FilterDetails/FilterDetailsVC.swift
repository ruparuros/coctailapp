//
//  FilterDetailsVC.swift
//  CoctailApp
//
//  Created by Uros Rupar on 11/16/23.
//

import UIKit

protocol FilterDelegate {
    func refreshFilter(selectedParametar: String, url: String)
}

class FilterDetailsVC: UIViewController {

    class var identifier: String { "FilterDetailsVC" }
    var url: String!
    var fiterCategoryParamtar: FilterParametarCategory!
    var selectedParametar: String!
   static var delegate: FilterDelegate?
    @IBOutlet weak var filterTableView: UITableView!
    var filterDetailsparametars: [FilterCategory]? {
        didSet {
            DispatchQueue.main.async {
                self.filterTableView.reloadData()
            }
        }
    }
    class func instantiate(url: String, filterParametar: FilterParametarCategory) -> FilterDetailsVC {
        let storyboard = UIStoryboard(name: "Filter", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: identifier) as! FilterDetailsVC
        vc.url = url
        vc.fiterCategoryParamtar = filterParametar
        DataManager.fetchData(url: vc.url, type: FilterData.self) { data in
            vc.filterDetailsparametars = data?.drinks
        }
        DispatchQueue.main.async {
            vc.filterTableView.reloadData()
        }
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib(nibName: "FilterTableViewCell", bundle: nil)
        filterTableView.register(nib, forCellReuseIdentifier: "FilterTableViewCell")
        setStatusBarColor()
        setMainBackground()
    }

}

extension FilterDetailsVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        filterDetailsparametars?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = UITableViewCell()
            cell.textLabel?.textAlignment = .center
            cell.textLabel?.textColor = .systemGray3
            cell.textLabel?.text = "filter drinks by \(self.fiterCategoryParamtar.rawValue)"
            cell.textLabel?.setDefaultFont(fontSize: 20)
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FilterTableViewCell", for: indexPath) as! FilterTableViewCell
        let item = self.filterDetailsparametars![indexPath.row]
        var parametarTitle  = {
            switch self.fiterCategoryParamtar {
            case .byAlcoholic:
                return item.strAlcoholic
            case .byCategory:
                return item.strCategory
            case .byIngradient:
                return item.strIngredient1
            case .byGlass:
                return item.strGlass
            case .firstLetter:
                return item.strGlass
            case .none:
                return ""
            
            }
        }()
        cell.filterParametarTitleLabel.text = parametarTitle
        cell.arrowIcon.isHidden = true
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = self.filterDetailsparametars![indexPath.row]
        var urlParametar: String = ""
            switch self.fiterCategoryParamtar {
            case .byAlcoholic:
                selectedParametar = item.strAlcoholic!
                urlParametar = "a=\(item.strAlcoholic!)"
            case .byCategory:
                selectedParametar = item.strCategory!
                urlParametar = "c=\(item.strCategory!)"
            case .byIngradient:
                selectedParametar = item.strIngredient1!
                urlParametar = "i=\(item.strIngredient1!)"
            case .byGlass:
                selectedParametar = item.strGlass!
                urlParametar = "g=\(item.strGlass!)"
            case .firstLetter:
                selectedParametar = item.strGlass!
                urlParametar = "g=\(item.strGlass!)"
            case .none:
                urlParametar = ""
            }
        
        tableView.deselectRow(at: indexPath, animated: true)
        let url = "https://www.thecocktaildb.com/api/json/v1/1/filter.php?\(urlParametar)".replacingOccurrences(of: " ", with: "_")
        DispatchQueue.main.async {
            FilterDetailsVC.delegate?.refreshFilter(selectedParametar: self.selectedParametar, url: url)
        }
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
}
