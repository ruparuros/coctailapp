//
//  AccountVC.swift
//  CoctailApp
//
//  Created by Uros Rupar on 11/13/23.
//

import UIKit

class AccountVC: UIViewController {

    private(set) var userService: UserService!
  
    var isEmailSaved = true
    var isPasswordSaved = true
    @IBOutlet weak var passwordTF: CustomTextField!
    @IBOutlet weak var emailTF: CustomTextField!
    @IBOutlet weak var logoutButtton: CustomButton!
    @IBOutlet weak var editEmailButton: UIButton!
    @IBOutlet weak var editPasswordButton: UIButton!
    @IBOutlet weak var emailwrapperView: UIStackView!
    
    @IBOutlet weak var passwordWrapperView: UIStackView!
    class var identifier: String { "AccountVC" }
    let repository = UserRepository()
    class func instantiate(userService: UserService) -> AccountVC {
        let storyboard = UIStoryboard(name: "Account", bundle: nil)
        
        let vc = storyboard.instantiateViewController(identifier: identifier) as! AccountVC
        vc.userService = userService
        return vc
    }
    
    class func instantiateWithNavigation(userService: UserService) -> UINavigationController {
        let vc = instantiate(userService: userService)
        return UINavigationController(rootViewController: vc)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigatonWithRightButtons()
        title = "accountTitle"~
        setNavigaton()
        setRegisterBackground()
        setUI()
    }
    
    func setUI() {
        emailwrapperView.layer.cornerRadius = 5
        emailwrapperView.layer.masksToBounds = true
        passwordWrapperView.layer.cornerRadius = 5
        passwordWrapperView.layer.masksToBounds = true
        logoutButtton.styleForSecondaryButton(title: "Logout")
        emailTF.text = self.userService.loadCurrentUser()?.username
        passwordTF.text = self.userService.loadCurrentUser()?.password
        
        emailTF.borderStyle = .none
        passwordTF.borderStyle = .none

        emailTF.isEnabled = false
        passwordTF.isEnabled = false
        
        editEmailButton.addAction(UIAction(handler: { _ in
            if self.isEmailSaved {
                self.editEmailButton.setImage(UIImage(systemName: "doc.on.clipboard"), for: .normal)
                self.emailTF.isEnabled = true
                self.isEmailSaved = false
            } else {
                self.warningAlert(warning: .edit(field: "email"), sender: self.editEmailButton)
            }
               
        }), for: .touchUpInside)
        
        editPasswordButton.addAction(UIAction(handler: { _ in
            if self.isPasswordSaved {
                self.editPasswordButton.setImage(UIImage(systemName: "doc.on.clipboard"), for: .normal)
                self.passwordTF.isEnabled = true
                self.isPasswordSaved = false
            } else {
                self.warningAlert(warning: .edit(field: "password"), sender: self.editPasswordButton)
            }
               
        }), for: .touchUpInside)
    }
    
    
    @IBAction func editUsernam(_ sender: UIButton) {
        sender.setImage(UIImage(systemName: "doc.on.clipboard"), for: .normal)
    }
    
    @IBAction func editPassword(_ sender: UIButton) {
        sender.setImage(UIImage(systemName: "doc.on.clipboard"), for: .normal)
    }
    
    @IBAction func logOut(_ sender: Any) {
        warningAlert(warning: .logout)
    }
    
    func presentLoginVCIfNeeded(){
        
        if self.tabBarController?.isBeingPresented == true{
            self.dismiss(animated: true, completion: nil)
        }else{
            let vc = LoginVC.instantiate(userService: self.userService)
            vc.modalPresentationStyle = .fullScreen
            vc.modalTransitionStyle = .crossDissolve
            present(vc, animated: true, completion: nil)
        }
    }

    func warningAlert(warning:Warning, sender: UIButton? = nil){
        let alert = UIAlertController(title: "info", message:warning.alertTitle, preferredStyle: UIAlertController.Style.alert)
        
        switch warning {
        case .delteUser:
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.destructive, handler: {action in
                self.userService.deleteCurrentUser()
                self.userService.logoutCurrentUser()
                self.presentLoginVCIfNeeded()
            }))
        case .logout:
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.destructive, handler: {action in
                self.userService.logoutCurrentUser()
                self.presentLoginVCIfNeeded()
            }))
        case .edit(field: let field):
            alert.title = "Edit"
            alert.message = "Are you sure you want to change \(field) data"
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.destructive, handler: {action in
                
                if sender == self.editEmailButton {
                    self.editEmailButton.setImage(UIImage(systemName: "pencil"), for: .normal)
                    self.emailTF.isEnabled = false
                    self.isEmailSaved = true
                } else {
                    self.editPasswordButton.setImage(UIImage(systemName: "pencil"), for: .normal)
                    self.passwordTF.isEnabled = false
                    self.isPasswordSaved = true
                }
                let currentUser = self.userService.loadCurrentUser()
                let userRealm = self.repository.getUserByEmail(email: currentUser!.username).first
                
                currentUser?.username = self.emailTF.text!
                currentUser?.password = self.passwordTF.text!
                
                self.repository.updateUser(user: userRealm!, email: currentUser!.username)
                self.userService.updateUser(currentUser!)
            }))
            
        }
        alert.addAction(UIAlertAction(title: "Cancle", style: UIAlertAction.Style.cancel, handler: {_ in
            self.emailTF.text = self.userService.loadCurrentUser()?.username
            self.passwordTF.text = self.userService.loadCurrentUser()?.password
        }))

        self.present(alert, animated: true, completion: nil)
    }
}
