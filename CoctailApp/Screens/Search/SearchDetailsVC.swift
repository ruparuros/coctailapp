//
//  SearchDetails.swift
//  CoctailApp
//
//  Created by Uros Rupar on 11/22/23.
//

import UIKit

class SearchDetailsVC: UIViewController {

    class var identifier: String { "SearchDetailsVC" }
    let collectionViewHeaderFooterReuseIdentifier = "HeaderView"
    @IBOutlet weak var searchCollectionView: UICollectionView!
    @IBOutlet weak var parametarLabel: UILabel!
    var searchText: String = ""
    var searchDrinks: [Drink] = [] {
        didSet {
            DispatchQueue.main.async {
                self.searchCollectionView.reloadData()
            }
        }
    }
        
    class func instantiate(text: String) -> SearchDetailsVC {
        let storyboard = UIStoryboard(name: "Filter", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: identifier) as! SearchDetailsVC
        DataManager.fetchData(url: "https://www.thecocktaildb.com/api/json/v1/1/search.php?s=\(text)", type: DrinkData.self) { data in
            vc.searchDrinks = data?.drinks ?? []
            vc.searchText = text
        }
        return vc
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib(nibName: "CoctailCollectionViewCell", bundle: nil)
        searchCollectionView.register(nib, forCellWithReuseIdentifier: "CoctailCollectionViewCell")
        searchCollectionView.register(UINib(nibName: collectionViewHeaderFooterReuseIdentifier, bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier:collectionViewHeaderFooterReuseIdentifier)

        searchCollectionView.register(UINib(nibName: collectionViewHeaderFooterReuseIdentifier, bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier:collectionViewHeaderFooterReuseIdentifier)
        setNavigaton()
        setMainBackground()
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SearchDetailsVC: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        searchDrinks.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CoctailCollectionViewCell", for: indexPath) as! CoctailCollectionViewCell
        
        cell.setCell(drink: (searchDrinks[indexPath.row]))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            if let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: collectionViewHeaderFooterReuseIdentifier, for: indexPath) as? HeaderView {
                headerView.parametarLabel.text = "search: \(self.searchText)"
                headerView.setCell()
                return headerView
            }
        default:
            return UICollectionReusableView()
        }

       
        return UICollectionReusableView()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        return CGSize(width: collectionView.frame.width, height: 70)
    }
}

extension SearchDetailsVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = Double(self.view.frame.width/2 - 10)
        let height = 253.0
        return CGSize(width: width, height: height)
    }
    
}
