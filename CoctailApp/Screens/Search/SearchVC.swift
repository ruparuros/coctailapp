//
//  SearchVC.swift
//  CoctailApp
//
//  Created by Uros Rupar on 11/19/23.
//

import UIKit

class SearchVC: UIViewController {

    class var identifier: String { "SearchVC" }
    @IBOutlet weak var searchBar: UISearchBar!
   
        
    class func instantiate() -> SearchVC {
        let storyboard = UIStoryboard(name: "Filter", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: identifier) as! SearchVC
        
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setMainBackground()
        setNavigaton()
        title = "Search Drinks"
        searchBar.backgroundColor = .mainColor
        for view in searchBar.subviews.last!.subviews {
            if type(of: view) == NSClassFromString("UISearchBarBackground") {
                view.alpha = 0.0
            }
        }
    }
}

extension SearchVC: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
    
        if let text =  searchBar.text, !text.isEmpty {
        
            let vc = SearchDetailsVC.instantiate(text: text)
            self.navigationController?.pushViewController(vc, animated: true)
            view.endEditing(false)
        }
    }
}




