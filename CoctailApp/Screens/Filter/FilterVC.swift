//
//  FilterVC.swift
//  CoctailApp
//
//  Created by Uros Rupar on 11/15/23.
//

import UIKit

class FilterVC: UIViewController {

    class var identifier: String { "FilterVC" }
    let filterParametars: [FilterParametarCategory] = [.byAlcoholic, .byCategory, .byGlass,.byIngradient, .firstLetter]
    @IBOutlet weak var filterTableView: UITableView!
    class func instantiate() -> FilterVC {
        let storyboard = UIStoryboard(name: "Filter", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: identifier) as! FilterVC
        
        return vc
    }
    
    class func instantiateWithNavigation() -> UINavigationController {
        let vc = instantiate()
        
        let navigation = UINavigationController(rootViewController: vc)
        return navigation
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigaton()
        
        title = " Filter Drinks"~
        let nib = UINib(nibName: "FilterTableViewCell", bundle: nil)
        filterTableView.register(nib, forCellReuseIdentifier: "FilterTableViewCell")
        setMainBackground()
    }
}

extension FilterVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        filterParametars.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = UITableViewCell()
            cell.textLabel?.textAlignment = .center
            cell.textLabel?.textColor = .systemGray3
            cell.textLabel?.text = "Filter Coctails by"
            cell.textLabel?.setDefaultFont(fontSize: 20)
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FilterTableViewCell", for: indexPath) as! FilterTableViewCell
            cell.filterParametarTitleLabel.text = self.filterParametars[indexPath.row - 1].rawValue
            return cell
            
            }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedParamtar = self.filterParametars[indexPath.row - 1]
        
        var parametar = {
            switch selectedParamtar {
            case .byAlcoholic:
                return "a"
            case .byCategory:
                return "c"
            case .byGlass:
                return "g"
            case .byIngradient:
                return "i"
            case .firstLetter:
                return "g"
                
            }
        }()
        
        tableView.deselectRow(at: indexPath, animated: true)
        let url = "https://www.thecocktaildb.com/api/json/v1/1/list.php?\(parametar)=list"
        let vc = FilterDetailsVC.instantiate(url: url, filterParametar: selectedParamtar)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
}
