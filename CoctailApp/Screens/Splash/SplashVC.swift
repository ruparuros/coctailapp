//
//  SplashVC.swift
//  CoctailApp
//
//  Created by Uros Rupar on 11/13/23.
//

import UIKit

class SplashVC: UIViewController {

    var userService:UserService = UserService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let _ = userService.loadCurrentUser(){
            let vc = MainTabBarVC.instantiate(userService: self.userService)
            vc.modalPresentationStyle = .fullScreen
            vc.modalTransitionStyle = .crossDissolve
            present(vc, animated:false,completion: nil)
            
        } else {
            let vc = LoginVC.instantiate(userService: self.userService)
            vc.modalPresentationStyle = .fullScreen
            vc.modalTransitionStyle = .crossDissolve
            present(vc, animated:false,completion: nil)
        }
        
        
    }


}
