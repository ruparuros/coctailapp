//
//  MainTabBarVC.swift
//  CoctailApp
//
//  Created by Uros Rupar on 11/13/23.
//

import UIKit

class MainTabBarVC: UITabBarController {
    
    private(set) var userService: UserService!
    
    let tabs = [
        ("house.fill", MenuItem.home),
        ("person.crop.circle", MenuItem.account),
        ("heart.fill", MenuItem.favourites),
    ]
    
    class var identifier: String { "MainTabBarVC" }
    
    class func instantiate(userService: UserService) -> MainTabBarVC {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let vc = storyboard.instantiateViewController(identifier: identifier) as! MainTabBarVC
        vc.userService = userService
        return vc
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewControllers = [
            HomeVC.instantiateWithNavigation(userService: self.userService),
            AccountVC.instantiateWithNavigation(userService: self.userService),
            FavouritesVC.instantiateWithNavigation(userService: self.userService)
        ]

        setupTabBarIcons()
        setupTabBar()
    }
    
    func setupTabBar() {
        tabBar.barTintColor = .secondary
        tabBar.isTranslucent = false
        tabBar.tintColor = UIColor.white
        tabBar.unselectedItemTintColor = UIColor.blue
   
        if #available(iOS 15, *) {
            let tabBarAppearance = UITabBarAppearance()
            tabBarAppearance.backgroundColor = .secondary
            tabBarAppearance.stackedLayoutAppearance.selected.titleTextAttributes = [.foregroundColor: UIColor.white, .font: UIFont(name: "caveat-regular", size: 15)]
            tabBarAppearance.stackedLayoutAppearance.normal.titleTextAttributes = [.foregroundColor: UIColor.white, .font: UIFont(name: "caveat-regular", size: 15)]
            tabBarAppearance.stackedLayoutAppearance.normal.iconColor = .white.withAlphaComponent(0.7)
            tabBar.standardAppearance = tabBarAppearance
            tabBar.scrollEdgeAppearance = tabBarAppearance
        }
    }
    
    func setupTabBarIcons() {
       
        guard let items = self.tabBar.items else {
            return
        }
        for (index, item) in items.enumerated() {
            item.image = UIImage(systemName: tabs[index].0)?.withRenderingMode(.alwaysTemplate)
            item.title = tabs[index].1.rawValue
        }
    }

}
