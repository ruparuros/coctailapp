//
//  CategoriesVC.swift
//  CoctailApp
//
//  Created by Uros Rupar on 11/13/23.
//

import UIKit

class CategoriesVC: UIViewController {

    class var identifier: String { "CategoriesVC" }
    
    class func instantiate() -> CategoriesVC {
        let storyboard = UIStoryboard(name: "Utils", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: identifier) as! CategoriesVC
        
        return vc
    }
    
    class func instantiateWithNavigation() -> UINavigationController {
        let vc = instantiate()
        
        let navigation = UINavigationController(rootViewController: vc)
        return navigation
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigaton()
        title = "categoriesTitle"~
        // Do any additional setup after loading the view.
    }

}
