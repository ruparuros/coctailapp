//
//  ViewController.swift
//  CoctailApp
//
//  Created by Uros Rupar on 11/13/23.
//

import UIKit
import RealmSwift
class HomeVC: UIViewController {
    
    private(set) var userService: UserService!
    var url: String = "https://www.thecocktaildb.com/api/json/v1/1/filter.php?c=Cocktail"
    class var identifier: String { "HomeVC" }
    var repository = UserRepository()
    @IBOutlet weak var captionView: UIView!
    @IBOutlet weak var coctailsCollectionView: UICollectionView!
    @IBOutlet weak var filterParametarLabel: UILabel!
    @IBOutlet weak var CaptionWraperView: UIView!
    
    var coctails: [Drink]! = []{
        didSet{
            DispatchQueue.main.async {
                self.coctailsCollectionView.reloadData()
            }
        }
    }
    class func instantiate(userService: UserService) -> HomeVC {
        let storyboard = UIStoryboard(name: "Utils", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: identifier) as! HomeVC
        vc.loadData()
        vc.userService = userService
        return vc
    }
    
    class func instantiateWithNavigation(userService: UserService) -> UINavigationController {
        let vc = instantiate(userService: userService)
        
        let navigation = UINavigationController(rootViewController: vc)
        
      
        return navigation
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib(nibName: "CoctailCollectionViewCell", bundle: nil)
        coctailsCollectionView.register(nib, forCellWithReuseIdentifier: "CoctailCollectionViewCell")
        
        title = "homeTitle"~
        FilterDetailsVC.delegate = self
        
        filterParametarLabel.text = "Coctails"
        setMainBackground()
        CaptionWraperView.backgroundColor = .mainColor
        captionView.layer.cornerRadius = 9
        CaptionWraperView.addBottomBorderWithColor(color: .customGrayColor, width: 1)
        CaptionWraperView.addTopBorderWithColor(color: .customGrayColor, width: 1)
        filterParametarLabel.setDefaultFont(fontSize: 18)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setNavigatonWithRightButtons()
        DispatchQueue.main.async {
            self.coctailsCollectionView.reloadData()
        }
    }
   
    func loadData(){
        DataManager.fetchData(url: self.url, type: DrinkData.self) { data in
            self.coctails = data?.drinks
            DispatchQueue.main.async {
                self.coctailsCollectionView.reloadData()
            }
        }
    }
}

extension HomeVC: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        coctails.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CoctailCollectionViewCell", for: indexPath) as! CoctailCollectionViewCell
        
        cell.setCell(drink: coctails[indexPath.row])
        cell.delegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.navigationController?.pushViewController(CoctailDetailVC.instantiate(drink: self.coctails[indexPath.row]), animated: true)
    }
}

extension HomeVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = Double(self.view.frame.width/2 - 10)
        let height = 253.0
        return CGSize(width: width, height: height)
    }
    
}

extension HomeVC: DrinkFavouritesDelegate {
    func appendToFavourites(drink: DrinkRealm) {
        let currentUserEmail = userService.loadCurrentUser()?.username
        
        let user = repository.getUserByEmail(email: currentUserEmail ?? "")
        let userRealm = UserRealm()
        userRealm.drinks = user.first!.drinks
        userRealm.email = user.first!.email
        repository.addDrink(drink: drink, user:userRealm )
    }

    func removeFromFavourites(drink: DrinkRealm) {
        let currentUserEmail = userService.loadCurrentUser()?.username
        let user = repository.getUserByEmail(email: currentUserEmail ?? "")
        let userRealm = UserRealm()
        userRealm.drinks = user.first!.drinks
        userRealm.email = user.first!.email
        repository.removeDrink(drink: drink, user: userRealm)
    }
}

extension HomeVC: FilterDelegate {
    func refreshFilter(selectedParametar: String, url: String) {
        self.url = url
        self.loadData()
        filterParametarLabel.text = selectedParametar
    }
    
    
}

