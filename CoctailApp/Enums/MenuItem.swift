//
//  MenuItem.swift
//  CoctailApp
//
//  Created by Uros Rupar on 11/13/23.
//

import Foundation

enum MenuItem: String {
    case home = "homeTitle"
    case account = "accountTitle"
    case favourites = "faouritesTitle"
    case unknown
}
