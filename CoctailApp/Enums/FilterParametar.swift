//
//  FilterParametar.swift
//  CoctailApp
//
//  Created by Uros Rupar on 11/16/23.
//

import Foundation

enum FilterParametarCategory: String {
    case byGlass = "Glass used"
    case byCategory = "Category"
    case byAlcoholic = "Alcoholic or not"
    case byIngradient = "Ingradient"
    case firstLetter = "first letter"
}
