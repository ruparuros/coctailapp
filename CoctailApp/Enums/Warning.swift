//
//  Warning.swift
//  CoctailApp
//
//  Created by Uros Rupar on 11/13/23.
//

import Foundation

enum Warning{
    case logout
    case delteUser
    case edit(field: String)
    var alertTitle:String{
        switch self {
        
        case .logout:
            return "Are You sure you want logout"
        case .delteUser:
            return "Are You sure you want to delete account"
        case .edit(let field):
            return "Are you sure you want to edit \(field) data"
        }
    }
}
